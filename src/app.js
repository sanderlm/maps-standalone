/* 
 * Copyright (c) 2018 RuneScape Wiki team
 *
 * SPDX-License-Identifier: GPL-3.0
 */

const MAP_BASE_ZOOM = 2;
const MAP_MAX_ZOOM = 5;
const MAP_MAX_NATIVE_ZOOM = 2;
const MAP_CENTER = [ 3225, 3219 ]; // Lumbridge
const MAP_BOUNDS = [ [0, 0], [12800, 12800] ];
const MAP_TILE_URL = 'http://167.99.150.82/maps/tiles/{z}_0_{x}_{-y}.png';

let _map;
let _tileLayer;


// 'DOMContentLoaded' may fire before your script has a chance to run, so check before adding a listener
if (document.readyState === "loading") {
	document.addEventListener("DOMContentLoaded", bootstrap);
} else {
	// 'DOMContentLoaded' already fired
	bootstrap();
}

function bootstrap() {
	var list = document.getElementsByClassName('map');
	for (var i = 0; i < list.length; i++) {
		setup(list[i]);
	}
}

function setup(domElem) {
	console.log("Setting up map...");

	L.CRS.Simple.infinite = false;
	L.CRS.Simple.projection.bounds = new L.Bounds(MAP_BOUNDS);

	// Setup map
	_map = L.map(domElem, {
		crs: L.CRS.Simple,
		maxBounds: MAP_BOUNDS,
		maxBoundsViscosity: 0.5,
		zoomControl: false, // Replace default zoom controls with our own
		attributionControl: false, // Remove copyright, but reference Leaflet in the options
	});

	// Setup map events
	_map.on('load', () => {
		setupLabels();
	});

	// Setup map controls
	setupControls();

	// Setup tiles
	setupTileLayer();

	let mapViewSet = false;

	// Check if request has highlighted bounds
	mapViewSet |= highlight(domElem.dataset.highlight);
	
	// Initialize map at default coordinates and zoom level
	if(!mapViewSet) {
		_map.setView(MAP_CENTER, MAP_BASE_ZOOM);
	}
}

// List of values are stored an array with [ y,x ] points
let locationLookup = new Map();
locationLookup.set('Blue Moon Inn', [ [ 3403,3218 ], [ 3403,3219 ], [ 3404,3219 ], [ 3404,3222 ], [ 3403,3222 ], [ 3403,3233 ], [ 3398,3233 ], [ 3397,3234 ], [ 3396,3234 ], [ 3395,3233 ], [ 3393,3233 ], [ 3393,3226 ], [ 3394,3225 ], [ 3394,3222 ], [ 3393,3221 ], [ 3393,3217 ], [ 3394,3216 ], [ 3397,3216 ], [ 3397,3218 ] ]);

let _mask;

function debugHighlight(e) {
	if(e instanceof KeyboardEvent && (e.key != 'Enter' && e.which != 13 && e.keyCode != 13))
        return;
	let val = (e instanceof KeyboardEvent ? e.currentTarget : document.getElementById('debugHighlight')).value;
	highlight(val);
}

function highlight(val) {
	if(val == undefined)
		return false;
	
	if(_mask) {
		_map.removeLayer(_mask);
	}

	let coords;
	if(locationLookup.has(val)) {
		coords = locationLookup.get(val);
	} else {
		// Attemp to parse value. Map string to array of coords.
		// Format: x,y/x,y/...
		coords = val.split('/').map(coord => coord.split(',').reverse());
	}

	let useOldStyle = false;
	if(useOldStyle) {
		_mask = L.polygon(coords, {
			weight: 5,
			color: '#041b7e',
			opacity: 0.9,
			fillColor: '#041b7e',
			fillOpacity: 0.5,
		}).addTo(_map);
	} else {
		_mask = L.mask(coords).addTo(_map);
	}

	// Create polygon with coordinates just to get corrent LatLngBounds
//	let bounds = new L.LatLngBounds(coords[0]);
	let bounds = L.polygon(coords).getBounds();
	if(bounds.isValid()) {
		_map.fitBounds(bounds, { padding: [50, 50] });
		return true;
	} else {
		console.log("Invalid coordinates: ", coords);
		return false;
	}
}

function setupLabels() {
	let layerLabels = L.layerGroup().addTo(_map);
	let layerIcons = L.canvasLayer().addTo(_map);

	let baseMaps = {
		"Overworld": _tileLayer
	};

	let overlayMaps = {
		"Locations": layerLabels,
		"Icons": layerIcons,
	};

	L.control.layers(baseMaps, overlayMaps).addTo(_map);

	let _iconLayers = [];
	let _iconLayersSub = [];

	loadLabels();
	loadIcons();

	function loadLabels() {
		fetch('data/labels.json')
			.then(response => {
				if(!response.ok) return []; // throw Error(response.statusText);
				return response.json();
			}).then(data => {
				for(let label of data) {
					// TODO: make these labels only appear at zoom level 3 or higher (>=200%)
					if(label.fontSize < 1) continue;

					let marker = L.textMarker([ label.y+0.5, label.x+0.5 ], label.text);
					layerLabels.addLayer(marker);
				}
			});
	}

	function loadIcons() {
		fetch('data/icons.json')
			.then(response => {
				if(!response.ok) return []; // throw Error(response.statusText);
				return response.json();
			}).then(data => {
				_buildIconLayers(data);
				_addVisLayers();
			});

		function _addVisLayers() {
			for(let layerName in _iconLayers) {
				let layer = _iconLayers[layerName];
				layerIcons.addLayer(layer);
			}
		}

		function _clearAllVisLayers() {
			for(let layerName in _iconLayers) {
				let layer = _iconLayers[layerName];
				layer.clearLayers();
			}
		}

		function _getCategoryIconLayer(category) {
			if(category in _iconLayers)
				return _iconLayers[category];

			let storageId = "category:" + name;
			let isVisible = true; // TODO: make some icons not visible by default

			let tmp;
			if(tmp = localStorage.getItem(storageId)) {
				isVisible = tmp === "true";
			}

			let layer = L.visibilityLayer({ visible: isVisible });
			_iconLayers[category] = layer;

			return layer;
		}

		function _getSubcategoryIconLayer(category, subcategory) {
			let storageId = "category:" + category + ":" + subcategory;

			if(storageId in _iconLayersSub)
				return _iconLayersSub[storageId];

			let baseLayer = _getCategoryIconLayer(category);
			let isVisible = baseLayer.getVisible();

			let tmp;
			if(tmp = localStorage.getItem(storageId)) {
				isVisible = tmp === "true";
			}

			let layer = L.visibilityLayer({ visible: isVisible });
			baseLayer.addLayer(layer);

			_iconLayersSub[storageId] = layer;
	//		this._createSubtypeLootButton(category, subcategory);

	/*		let self = this;
			$("#subloot_" + subcategory).change(function() {
				self._iconLayersSub[subcategory].setVisible(this.checked);
				localStorage.setItem(storageId, this.checked, {
					expires: self.options.cookieExpire
				});
			})
	*/
			return layer;
		}

		function _buildIconLayers(data) {
			for(let label of data) {
				// TODO: implement levels
	//			if(label.plane && label.plane != 0) continue;

				let layer = _getSubcategoryIconLayer(label.category || 'others', label.subcategory || label.id);

				let markerOptions = {
					icon: _getIcon('data/icons/' + label.id + '.png', [ label.width, label.height ]),
					clickable: false,
					visible: false,
					zIndexOffset: 0
				};

	/*			if(this.options.individualScaleFactor.hasOwnProperty(marker.subcategory)) {
					markerOptions.scaleFactor = this.options.individualScaleFactor[marker.subcategory];
				}
	*/
				let marker = new L.Marker([ label.y, label.x ], markerOptions);
				layer.addLayer(marker);
			}
		}

		function _getIcon(url, size, offset = [0,0]) {
			return L.divIcon({
				iconUrl: url,
				iconSize: size,
				spriteOffset: offset
			})
		}
	}
}

function setupControls() {
	L.control.customZoom().addTo(_map);
//	L.control.help().addTo(_map);
//	L.control.options().addTo(_map);
}

function setupTileLayer() {
	_tileLayer = L.tileLayer(MAP_TILE_URL, {
		minZoom: -3,
		bounds: MAP_BOUNDS,
		maxNativeZoom: MAP_MAX_NATIVE_ZOOM,
		maxZoom: MAP_MAX_ZOOM,
		attribution: 'Map data &copy; <a href="http://runescape.wikia.com">RuneScape Wiki</a>',
	}).addTo(_map);
}



/*
 * Helper functions
 */
function createElement(element, html, title, className, container) {
	let elem = L.DomUtil.create(element, className, container);
	elem.innerHTML = html;
	elem.title = title;

	// Will force screen readers like VoiceOver to read this as "Zoom in - button"
	elem.setAttribute('role', 'button');
	elem.setAttribute('aria-label', title);

	return elem;
}

function createButton(_this, html, title, className, container, fn = undefined) {
	let link = createElement('a', html, title, className, container);
	link.href = '#';

	L.DomEvent.disableClickPropagation(link);
	L.DomEvent.on(link, 'click', L.DomEvent.stop);
	if(fn) L.DomEvent.on(link, 'click', fn, _this);
	L.DomEvent.on(link, 'click', _this._refocusOnMap, _this);

	return link;
}



/*
 * Leaflet components
 */
L.Map.include({
	getBaseZoom: function() {
		return MAP_BASE_ZOOM;
	},

	getZoomPercentage: function(zoom = this.getBaseZoom()) {
		return 1 / this.getZoomScale(zoom);
	},
});

L.Control.CustomZoom = L.Control.Zoom.extend({
	options: {
		position: 'topright',

		zoomInText: '+',// + '&#x2b', // The text set on the 'zoom in' button.
		zoomInTitle: 'Zoom in', // The title set on the 'zoom in' button.
		zoomOutText: '−',// − '&#x2212;', // The text set on the 'zoom out' button.
		zoomOutTitle: 'Zoom out' // The title set on the 'zoom out' button.
	},

	onAdd: function(map) {
		let containerName = 'leaflet-control-zoom',
		container = L.DomUtil.create('div', containerName + ' leaflet-bar'),
		options = this.options;

		this._zoomInButton  = createButton(this, options.zoomInText, options.zoomInTitle, containerName + '-in', container, this._zoomIn);
		this._zoomLevel     = createButton(this, '', 'Zoom level', containerName + '-level', container, this._resetZoom);
		this._zoomOutButton = createButton(this, options.zoomOutText, options.zoomOutTitle, containerName + '-out', container, this._zoomOut);

		this._updateDisabled();
		map.on('zoomend zoomlevelschange', this._updateDisabled, this);

		return container;
	},

	_updateDisabled: function() {
		L.Control.Zoom.prototype._updateDisabled.call(this);

		// Update displayed zoom level
		this._zoomLevel.textContent = (this._map.getZoomPercentage() * 100) + '%';
	},

	_resetZoom: function() {
		this._map.setZoom(this._map.getBaseZoom());
	}
});
L.control.customZoom = opts => new L.Control.CustomZoom(opts);

L.Control.Help = L.Control.extend({
	options: {
		position: 'topright',

		buttonText: '<i class="fa fa-question"></i>', // The text set on the 'options' button.		
		buttonTitle: 'Help', // The title set on the 'options' button.
	},

	onAdd: function(map) {
		let containerName = 'leaflet-control-help',
		container = L.DomUtil.create('div', containerName + ' leaflet-bar'),
		options = this.options;

		this._optionsButton = helper.createButton(this, options.buttonText, options.buttonTitle, containerName + '-button', container);
		this._optionsButton.dataset.micromodalTrigger = 'modal-help';

		return container;
	},

	onRemove: function(map) {
		// Do nothing
	}
});
L.control.help = opts => new L.Control.Help(opts);



L.TextIcon = L.DivIcon.extend({
	options: {
		className: 'leaflet-div-icon marker-text-icon',
		iconSize: null,
	}
});
L.textIcon = (text, opts = {}) => new L.TextIcon(Object.assign({ html: text }, opts));

L.TextMarker = L.Marker.extend({
	options: {
	},

	initialize: function(latlng, options) {
		L.setOptions(this, options);
		this._latlng = L.latLng(latlng);
		this._scale = 1;
	},

	_initIcon: function() {
		L.Marker.prototype._initIcon.call(this);

		// Set up icon anchor dynamically, based on (now computed) icon bounds
		this._icon.iconAnchor = L.point([
			this._icon.offsetWidth / 2,
			this._icon.offsetHeight / 2
		]);

//		this._icon.popupAnchor = this._icon.iconAnchor;		
	},

	onAdd: function(map) {
		this._scale = map.getZoomPercentage();
		L.Marker.prototype.onAdd.call(this, map);
	},

	_animateZoom: function (e) {
		this._scale = this._map.getZoomScale(e.zoom, MAP_BASE_ZOOM);

		let pos = this._map._latLngToNewLayerPoint(this._latlng, e.zoom, e.center).round();
		this._setPos(pos);
	},

	_setPos: function(pos) {
		pos = pos.subtract(this._icon.iconAnchor.multiplyBy(this._scale));

		L.DomUtil.setTransform(this._icon, pos, this._scale);

		if (this._shadow) {
			L.DomUtil.setTransform(this._shadow, pos, this._scale);
		}

		this._zIndex = pos.y + this.options.zIndexOffset;
		this._resetZIndex();
	},
});
L.textMarker = (latlng, text) => new L.TextMarker(latlng, { icon: L.textIcon(text), alt: text });

// credits: https://github.com/turban/Leaflet.Mask
L.Mask = L.Polygon.extend({
	options: {
		stroke: false,
		color: 'black',
		fillOpacity: 0.5,
		clickable: false
	},

	initialize: function(latLngs, options) {
		let outerBounds = new L.LatLngBounds(MAP_BOUNDS);
		let outerBoundsLatLngs = [
			outerBounds.getSouthWest(),
			outerBounds.getNorthWest(),
			outerBounds.getNorthEast(),
			outerBounds.getSouthEast()
		];

		L.Polygon.prototype.initialize.call(this, [ outerBoundsLatLngs, latLngs ], options);	
	},

});
L.mask = (latlng, options) => new L.Mask(latlng, options);


L.CanvasLayer = L.GridLayer.extend({

	statics: {
		Images: {}
	},

	initialize: function(options) {
		L.setOptions(this, options);

		this._layerList = [];
		this._layerGrid = [];
		this._loadingImages = false;
		this._loadingFinishedCallbacks = [];

		this.options.maxNativeZoom = _tileLayer.options.maxNativeZoom;
		this.options.maxZoom = _tileLayer.options.maxZoom;
	},

	onAdd: function(map) {
		L.GridLayer.prototype.onAdd.call(this, map);

		this._currentZoom = map.getZoom();
	},

	addLayer: function(layer) {
		if (!(layer instanceof L.VisibilityLayer)) {
			console.log("FIX: CanvasLayer.addLayer(layer) only accepts VisibilityLayer as argument");
			return this;
		}

		let id = L.Util.stamp(layer);
		this._layerList[id] = layer;
		layer._canvasLayer = this;
		
		for(let image of layer.getImages()) {
			this._loadImage(image);
		}
		this._addLayer(layer);

		L.GridLayer.prototype.redraw.call(this);

		return this;
	},

	createTile: function(coords, doneCallback) {
		let canvas = document.createElement("canvas");
		let canvasContext = canvas.getContext("2d");

		let size = this.getTileSize();
		canvas.width = size.x;
		canvas.height = size.y;
		
		let zoom = coords.z;
		if(this._currentZoom != zoom) {
			this._currentZoom = zoom;
			this._rebuildLayerGrid();
		}

		let layerGrid = this._layerGrid[coords.x] ? this._layerGrid[coords.x][coords.y] : [];
		if(!layerGrid || layerGrid.length == 0) {
			return canvas;
		}

		// Only draw tile once all images have been loaded
		if(this._loadingImages > 0) {
			this._loadingFinishedCallbacks.push({
				method: this._drawTile,
				parameters: [coords, doneCallback, canvas, canvasContext, size, layerGrid]
			});
		} else {
			this._drawTile(coords, doneCallback, canvas, canvasContext, size, layerGrid);
		}

		return canvas;
	},

	_drawTile: function(coords, doneCallback, canvas, canvasContext, tileSize, layerGrid) {
		// Sort layers
		layerGrid.sort(function(a, b) {
			a = a.options.zIndexOffset + a._latlng.lat;
			b = b.options.zIndexOffset + b._latlng.lat;
			return a > b ? 1 : b > a ? -1 : 0
		});

//		let scale = this._map.getZoomPercentage(coords.z) / 2;
		let scale = this._map.getZoomScale(MAP_BASE_ZOOM, coords.z);

		let tilePos = L.point([ coords.x * tileSize.x, coords.y * tileSize.y ]);
		for(let layer of layerGrid) {
			if(!layer._visibilityLayer.getVisible()) {
				continue;
			}

			let iconOptions = layer.options.icon.options;
			let iconUrl = iconOptions.iconUrl;

			let icon = this._getImage(iconUrl);
			if (icon) {
				let size = L.point(iconOptions.iconSize);
				let offset = L.point(iconOptions.spriteOffset || [0,0]);
				let bounds = layer._bounds;
				let sizeScaled = layer.getSizeForScaling(coords.z);

				if(scale <= 1) {
					offset = ((size.y/2) * scale);
				} else {
					offset = (size.y/2);
				}

				// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
				//    drawImage(image, sourceX, sourceY, sourceWidth, sourceHeight, destinationX, destinationY, destinationWidth, destinationHeight)
				canvasContext.drawImage(
					icon,/*
					offset.x, // sprite
					offset.y, // sprite
					size.x,
					size.y,*/
					bounds.min.x - tilePos.x,
					bounds.min.y - tilePos.y - offset,
					sizeScaled.x,
					sizeScaled.y
				);
			} else {
				console.log("FIX: should not happen. Images should already be loaded. iconUrl: " + iconUrl);
			}
		}

		// TODO: Doesn't load new canvas without the 1ms timeout
		setTimeout(() => doneCallback(null, canvas), 1);
	},

	_fireLoadingCallbacks: function(self) {
		if(self._loadingImages) {
			return;
		}

		for(let callback of self._loadingFinishedCallbacks) {
			let args = Array.prototype.slice.call(callback.parameters, 0);
			// TODO: check if this is being called
			callback.method.apply(this, args);
		}

		self._loadingFinishedCallbacks = [];
	},

	_loadImage: function(iconUrl) {
		if (L.CanvasLayer.Images[iconUrl]) {
			return;
		}

		this._loadingImages++;
		
		let img = new Image;
		let self = this;

		img.onload = function() {
			this._loaded = true;
			self._loadingImages--;
			self._fireLoadingCallbacks(self);
		};
		img.onerror = function() {
			console.log("FIX: Unable to load image. imageUrl: " + iconUrl);
			L.CanvasLayer.Images[iconUrl] = false;
			self._loadingImages--;
			self._fireLoadingCallbacks(self);
		};

		img.src = iconUrl;
		L.CanvasLayer.Images[iconUrl] = img;
	},

	_getImage: function(iconUrl) {
		let img = L.CanvasLayer.Images[iconUrl];
		if(img) {
			if(img._loaded) {
				return img;
			} else {
				return false;
			}
		} else {
			console.log("FIX: should not happen? has the iconUrl changed? iconUrl: " + iconUrl);
			return false;
		}
	},

	_addLayer: function(layer) {
		let tileSize = this.options.tileSize;

		if(typeof layer.eachLayer === 'function') {
			layer.eachLayer(_layer => {
				if(_layer instanceof L.Marker) {
					this._addMarker(_layer, tileSize);
				} else if(_layer instanceof L.VisibilityLayer) {
					this._addLayer(_layer);
				} else {
					console.log("FIX: CanvasLayer only accepts Markers or VisibilityLayers.", _layer);
				}
			});
		}
	},

	_addMarker: function(layer, tileSize) {
		var pos = this._map.options.crs.latLngToPoint(layer.getLatLng(), this._currentZoom);
		let size = layer.getSizeForScaling(this._currentZoom);
		let center = L.point([size.x / 2, size.y / 2]);
		
		layer._bounds = L.bounds([
			[pos.x - center.x, pos.y - center.y],
			[pos.x + (size.x - center.x), pos.y + (size.y - center.y)]
		]);

		let boundsMin = L.point([Math.floor(layer._bounds.min.x / tileSize), Math.floor(layer._bounds.min.y / tileSize)]);
		let boundsMax = L.point([Math.floor(layer._bounds.max.x / tileSize), Math.floor(layer._bounds.max.y / tileSize)]);
		for (let y = boundsMin.y; y <= boundsMax.y; y++) {
			for (let x = boundsMin.x; x <= boundsMax.x; x++) {
				this._addMarkerToList(layer, L.point([x, y]));
			}
		}
	},

	_addMarkerToList: function(layer, coords) {
		if(!this._layerGrid[coords.x]) {
			this._layerGrid[coords.x] = {};
		}

		if(!this._layerGrid[coords.x][coords.y]) {
			this._layerGrid[coords.x][coords.y] = [];
		}

		this._layerGrid[coords.x][coords.y].push(layer);
	},

	_rebuildLayerGrid: function() {
		this._layerGrid = {};
		for(let id in this._layerList) {
			let layer = this._layerList[id];
			this._addLayer(layer);
		}
	}
});
L.canvasLayer = opts => new L.CanvasLayer(opts);

L.VisibilityLayer = L.LayerGroup.extend({
	options: {
		visible: true
	},

	initialize: function(options) {
		options = L.setOptions(this, options);
		L.LayerGroup.prototype.initialize.call(this, options);
	},

	// TODO: needed?
	onAdd: function(map) {
		L.LayerGroup.prototype.onAdd.call(this, map);
	},

	setVisible: function(newVisible, e) {
		let oldVisible = this.options.visible;
		this._setVisible(newVisible, e);

		if(oldVisible !== newVisible) {
			let canvasLayer = this._getCanvasLayer();
			if(canvasLayer !== null) {
				canvasLayer.redraw();
			}
		}
	},

	_setVisible: function(visible, e = undefined) {
		if(typeof e != "undefined") {
			this.eachLayer(function(layer) {
				if(layer instanceof L.VisibilityLayer) {
					layer._setVisible(visible, e);
				}
			})
		}

		this.options.visible = visible;
	},

	getVisible: function() {
		return this.options.visible;
	},

	getImages: function() {
		let images = [];
		
		this.eachLayer(function(layer) {
			if (layer instanceof L.Marker) {
				images.push(layer.options.icon.options.iconUrl);
			} else if (layer instanceof L.VisibilityLayer) {
				// Merge arrays
				images.push.apply(images, layer.getImages());
			}
		})

		return images;
	},

	addLayer: function(layer) {
		if(layer instanceof L.Marker) {
			L.LayerGroup.prototype.addLayer.call(this, layer);
			layer._visibilityLayer = this;
		} else if(layer instanceof L.VisibilityLayer) {
			L.LayerGroup.prototype.addLayer.call(this, layer);
			layer._superLayer = this;
		} else {
			console.log("FIX: VisibilityLayer only accepts Markers or VisibilityLayers.", layer);
		}

		return this;
	},

	_getCanvasLayer: function() {
		let self = this;
		do {
			if(self._canvasLayer instanceof L.CanvasLayer)
				return self._canvasLayer;

			self = self._superLayer;
		} while (self instanceof L.VisibilityLayer);

		return null;
	}
});
L.visibilityLayer = opts => new L.VisibilityLayer(opts);

L.Marker.include({
	getSizeForScaling: function(zoom) {
		let scale = 1;
		if(typeof this.options.scaleFactor != "undefined") {
			scale = this.options.scaleFactor;
		}

		let width = Math.floor(this.options.icon.options.iconSize[0] * scale);
		let height = Math.floor(this.options.icon.options.iconSize[1] * scale);
		return L.point(width, height);
	}
});