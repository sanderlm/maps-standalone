# RuneScape Maps (standalone version)

Simpler version of the maps project, intended to be used in the RuneScape Wiki. The code in this repository does not support older browsers, as to maintain the code in here simple and readable.

A live version of the code can be seen [here](https://rs-wiki.gitlab.io/maps-standalone/).